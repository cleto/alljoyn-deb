# -*- mode:make -*-

GTEST_DIR=/usr/src/gtest

debian/build_c_stamp: debian/build_cpp_stamp
	scons $(SCONS_ARGS) BINDINGS=c GTEST_DIR=$(GTEST_DIR) \
		WS=off BR=off BUILD_SERVICES_SAMPLES=off  \
		-C core/alljoyn --debug=presub
	mv $(BUILD_DIR)/c/lib/liballjoyn_c.so \
		$(BUILD_DIR)/c/lib/liballjoyn.so.$(VERSION)
	ln -sf liballjoyn.so.$(VERSION) \
		$(BUILD_DIR)/c/lib/liballjoyn.$(SONAME)
	ln -sf liballjoyn.so.$(SONAME) \
		$(BUILD_DIR)/c/lib/liballjoyn.so
	> $@
