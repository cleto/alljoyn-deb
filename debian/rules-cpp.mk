# -*- mode:make -*-

GTEST_DIR=/usr/src/gtest

debian/build_cpp_stamp:
	scons $(SCONS_ARGS) BINDINGS=cpp GTEST_DIR=$(GTEST_DIR) \
		WS=off BUILD_SERVICES_SAMPLES=off -C core/alljoyn --debug=presub -j4
	mv $(BUILD_DIR)/cpp/lib/liballjoyn.so \
		$(BUILD_DIR)/cpp/lib/liballjoyn++.so.$(VERSION)
	ln -sf liballjoyn++.so.$(VERSION) \
		$(BUILD_DIR)/cpp/lib/liballjoyn++.so.$(SONAME)
	ln -sf liballjoyn++.so.$(SONAME) \
		$(BUILD_DIR)/cpp/lib/liballjoyn++.so
	> $@
